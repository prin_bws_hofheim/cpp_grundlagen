#-------------------------------------------------
#
# Project created by QtCreator 2017-08-01T11:01:24
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dame_2
TEMPLATE = app


SOURCES += main.cpp\
        spieldamegui.cpp \
    spieldameablaufsteuerung.cpp \
    spieldamedatenhalde.cpp

HEADERS  += spieldamegui.h \
    spieldameablaufsteuerung.h \
    spieldamedatenhalde.h

FORMS    += spieldamegui.ui
