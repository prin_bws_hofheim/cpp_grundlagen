#include "spieldamedatenhalde.h"

/**
 * @brief Konstruktor - Daten auf defaut setzen
 */
SpielDameDatenhalde::SpielDameDatenhalde()
{
    aktiverSpieler = spielerTypSpieler1;

    for(int i = 0; i<8; i++)
    {
        for(int j = 0; j < 8; j++)
        {
            spielfeld[i][j].spieler = spielertypKein;
            spielfeld[i][j].spielstein = spielsteinKein;
        }
    }
}

SpielDameDatenhalde::spielerTyp SpielDameDatenhalde::getAktiverSpieler()
{
    return aktiverSpieler;
}

/**
 * @brief von 1 nach 2 und umgekehrt
 */
void SpielDameDatenhalde::aktiverSpielerWechseln()
{
    if(spielerTypSpieler1 == aktiverSpieler)
        aktiverSpieler = spielerTypSpieler2;
    else
        aktiverSpieler = spielerTypSpieler1;
}

SpielDameDatenhalde::spielerTyp SpielDameDatenhalde::getSpielfeldInhalt(int reihe, int spalte)
{
    return spielfeld[reihe][spalte].spieler;
}

/**
 * @brief Feld aufraeumen.
 * @todo optimieren!
 */
void SpielDameDatenhalde::spielNeustarten()
{
    spielfeld[0][1].spieler = spielerTypSpieler1;
    spielfeld[0][1].spielstein = spielsteinTypStein;
    spielfeld[0][3].spieler = spielerTypSpieler1;
    spielfeld[0][3].spielstein = spielsteinTypStein;
    spielfeld[0][5].spieler = spielerTypSpieler1;
    spielfeld[0][5].spielstein = spielsteinTypStein;
    spielfeld[0][7].spieler = spielerTypSpieler1;
    spielfeld[0][7].spielstein = spielsteinTypStein;
    spielfeld[1][0].spieler = spielerTypSpieler1;
    spielfeld[1][0].spielstein = spielsteinTypStein;
    spielfeld[1][2].spieler = spielerTypSpieler1;
    spielfeld[1][2].spielstein = spielsteinTypStein;
    spielfeld[1][4].spieler = spielerTypSpieler1;
    spielfeld[1][4].spielstein = spielsteinTypStein;
    spielfeld[1][6].spieler = spielerTypSpieler1;
    spielfeld[1][6].spielstein = spielsteinTypStein;
    spielfeld[2][1].spieler = spielerTypSpieler1;
    spielfeld[2][1].spielstein = spielsteinTypStein;
    spielfeld[2][3].spieler = spielerTypSpieler1;
    spielfeld[2][3].spielstein = spielsteinTypStein;
    spielfeld[2][5].spieler = spielerTypSpieler1;
    spielfeld[2][5].spielstein = spielsteinTypStein;
    spielfeld[2][7].spieler = spielerTypSpieler1;
    spielfeld[2][7].spielstein = spielsteinTypStein;

    spielfeld[5][0].spieler = spielerTypSpieler2;
    spielfeld[5][0].spielstein = spielsteinTypStein;
    spielfeld[5][2].spieler = spielerTypSpieler2;
    spielfeld[5][2].spielstein = spielsteinTypStein;
    spielfeld[5][4].spieler = spielerTypSpieler2;
    spielfeld[5][4].spielstein = spielsteinTypStein;
    spielfeld[5][6].spieler = spielerTypSpieler2;
    spielfeld[5][6].spielstein = spielsteinTypStein;
    spielfeld[6][1].spieler = spielerTypSpieler2;
    spielfeld[6][1].spielstein = spielsteinTypStein;
    spielfeld[6][3].spieler = spielerTypSpieler2;
    spielfeld[6][3].spielstein = spielsteinTypStein;
    spielfeld[6][5].spieler = spielerTypSpieler2;
    spielfeld[6][5].spielstein = spielsteinTypStein;
    spielfeld[6][7].spieler = spielerTypSpieler2;
    spielfeld[6][7].spielstein = spielsteinTypStein;
    spielfeld[7][0].spieler = spielerTypSpieler2;
    spielfeld[7][0].spielstein = spielsteinTypStein;
    spielfeld[7][2].spieler = spielerTypSpieler2;
    spielfeld[7][2].spielstein = spielsteinTypStein;
    spielfeld[7][4].spieler = spielerTypSpieler2;
    spielfeld[7][4].spielstein = spielsteinTypStein;
    spielfeld[7][6].spieler = spielerTypSpieler2;
    spielfeld[7][6].spielstein = spielsteinTypStein;

    aktiverSpieler = spielerTypSpieler1;
}

/**
 * @brief Liste aller Spielsteine ausgeben
 * @return Liste der Steine + Spieler
 *
 * Die Methode sammelt alle Daten der Steine auf dem Brett
 * und gibt sie als Liste (Vector) zurueck. Sie fuehrt die Info zur Pos und den Inhalt
 * Stein / Dame + Spieler zusammen
 */
vector<SpielDameDatenhalde::spielfeldEintrag> SpielDameDatenhalde::steineListeHolen()
{
    vector<spielfeldEintrag> spielfeldVector;

    for (int i = 0; i < 8; i++) // Reihe
    {
        for(int j = 0; j < 8; j++) // Spalte
        {
            spielfeldEintrag tempSpielfeld;

            tempSpielfeld.reihe = i;
            tempSpielfeld.spalte = j;
            tempSpielfeld.spieler = spielfeld[i][j].spieler;
            tempSpielfeld.spielstein = spielfeld[i][j].spielstein;
            spielfeldVector.push_back(tempSpielfeld);
        }
    }
    return spielfeldVector;
}

/**
 * @brief Stein von a nach b
 * @param reiheVorher
 * @param spalteVorher
 * @param reiheNachher
 * @param spalteNachher
 *
 * aendert den Eintrag im Array des Spielfelds
 */
void SpielDameDatenhalde::steinVersetzen(int reiheVorher, int spalteVorher, int reiheNachher, int spalteNachher)
{
    spielfeldBelegung tempEintrag;

    tempEintrag = spielfeld[reiheVorher][spalteVorher];
    spielfeld[reiheNachher][spalteNachher] = tempEintrag;

    // altes Feld raeumen!
    tempEintrag.spieler = spielertypKein;
    tempEintrag.spielstein = spielsteinKein;
    spielfeld[reiheVorher][spalteVorher] = tempEintrag;
}

/**
 * @brief wenn er geschlagen wurde ...
 * @param reiheVorher
 * @param spalteVorher
 *
 * loescht den Spielstein aus der Liste
 */
void SpielDameDatenhalde::steinEntfernen(int reiheVorher, int spalteVorher)
{
    spielfeldBelegung tempEintrag;

    // altes Feld raeumen!
    tempEintrag.spieler = spielertypKein;
    tempEintrag.spielstein = spielsteinKein;
    spielfeld[reiheVorher][spalteVorher] = tempEintrag;
}
