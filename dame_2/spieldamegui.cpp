#include "spieldamegui.h"
#include "ui_spieldamegui.h"

SpielDameGui::SpielDameGui(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::SpielDameGui)
{
    ui->setupUi(this);

    //    setGeometry(0, 0, 1000, 1000);
    QObject::connect(ui->btnResetGame, &QPushButton::clicked, this, &SpielDameGui::spielNeustarten);

    ablaufsteuerung = new SpielDameAblaufsteuerung(this);
}

SpielDameGui::~SpielDameGui()
{
    delete ui;
}

void SpielDameGui::spielNeustarten()
{
    ablaufsteuerung->spielNeustarten();
    brettNeuZeichnen();
}

/**
 * @brief SpielDameGui::brettNeuZeichnen
 *
 * löscht alle alten Einträge im Form,
 * zeichnet das Spielbrett als quadrate und Linien
 * legt Spielsteine als Label an, zeigt Bilder an
 * aktualisiert Beschriftung (Spielstand, aktueller Spieler, ...)
 */
void SpielDameGui::brettNeuZeichnen()
{
    spielfeldBelegungGui tempEntry;
    QLabel* tempLabel = 0;
    vector<SpielDameDatenhalde::spielfeldEintrag> steineListe;
    SpielDameDatenhalde::spielfeldEintrag tempEntrySpielfeldbelegung;
    int posX = 0;
    int posY = 0;

    if(0 != spielfeldBelegung.size())
    {
        for (std::vector<spielfeldBelegungGui>::iterator it = spielfeldBelegung.begin() ; it != spielfeldBelegung.end(); ++it)
        {
            tempEntry = *it;
            tempEntry.spielfeldLabel->hide();
            delete tempEntry.spielfeldLabel;
        }
    }
    spielfeldBelegung.clear();
    //vector leer -------

    steineListe = ablaufsteuerung->steineListeHolen();

    //Liste der Spielsteine in Gui laden
    if(0 == steineListe.size())
        return;

    for (std::vector<SpielDameDatenhalde::spielfeldEintrag>::iterator it = steineListe.begin() ; it != steineListe.end(); ++it)
    {
        tempEntrySpielfeldbelegung = *it;
        tempEntry.spielfeldLabel = 0;
        tempEntry.reihe = (*it).reihe;
        tempEntry.spalte = tempEntrySpielfeldbelegung.spalte;

        tempLabel = new QLabel(this);
        tempEntry.spielfeldLabel = tempLabel;

        posX = SpielDameDatenhalde::feldBreiteGui * (*it).spalte + 10 + SpielDameDatenhalde::feldBreiteGui / 2;
        posY = SpielDameDatenhalde::feldBreiteGui * (*it).reihe + 10;
        tempEntry.spielfeldLabel->setGeometry(posX,posY, 80, 80);

        tempEntry.spielfeldLabel->show();

        if(tempEntrySpielfeldbelegung.spieler == SpielDameDatenhalde::spielerTypSpieler1)
        {
            tempEntry.spielfeldLabel->setText("1");
        }
        if(tempEntrySpielfeldbelegung.spieler == SpielDameDatenhalde::spielerTypSpieler2)
        {
            tempEntry.spielfeldLabel->setText("2");
        }
        spielfeldBelegung.push_back(tempEntry);
    }
}

/**
 * @brief SpielDameGui::mousePressEvent
 * @param event
 *
 * nimmt MouseClick an, berechnet die Reihe und Spalte, ruft
 * Ablaufsteuerung auf.
 */
void SpielDameGui::mousePressEvent(QMouseEvent *event)
{
    int posX = 0;
    int posY = 0;
    int brettReihe = -1;
    int brettSpalte = -1;

    posX = event->x();
    posY = event->y();

    brettSpalte = (posX - 10) / SpielDameDatenhalde::feldBreiteGui;
    brettReihe = (posY - 10) / SpielDameDatenhalde::feldBreiteGui;

    qDebug() << "Reihe " << brettReihe << " Spalte " << brettSpalte;

    ablaufsteuerung->klickAufFeldBearbeiten(brettReihe, brettSpalte);
}

/**
 * @brief paintEvent
 * @param event
 *
 * zeichnet die Quadrate aufs Form; optimierungsfähig :)
 */
void SpielDameGui::paintEvent(QPaintEvent *event)
{
    QPainter painter(this);

    //Linien aussen ...
    QLineF line(10, 10, 10, 8*SpielDameDatenhalde::feldBreiteGui+10);
    painter.drawLine(line);
    line.setLine(10, 10, 810, 10);
    painter.drawLine(line);
    line.setLine(810, 10, 810, 810);
    painter.drawLine(line);
    line.setLine(10, 810, 810, 810);
    painter.drawLine(line);

    //Quadrate
    QRect rect;
    rect.setWidth(SpielDameDatenhalde::feldBreiteGui);
    rect.setHeight(SpielDameDatenhalde::feldBreiteGui);

    rect.moveLeft(10);
    painter.setBrush(Qt::darkBlue);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * i * 2);
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui * 2);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * i * 2);
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui * 4);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * i * 2);
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui * 6);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * i * 2);
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui );
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * (i * 2+1));
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui * 3);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * (i * 2+1));
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui * 5);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * (i * 2+1));
        painter.drawRect(rect);
    }

    rect.moveLeft(10 + SpielDameDatenhalde::feldBreiteGui * 7);
    for(int i = 0; i < 4; i++)
    {
        rect.moveTop(10 + SpielDameDatenhalde::feldBreiteGui * (i * 2+1));
        painter.drawRect(rect);
    }
}

void SpielDameGui::on_btnLoadImagePlayer1_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "/home",
                                                    tr("Images (*.png *.xpm *.jpg)"));

    bildSpieler1.load(fileName);
}

void SpielDameGui::on_btnLoadImagePlayer2_clicked()
{
    QString fileName = QFileDialog::getOpenFileName(this, tr("Open File"),
                                                    "/home",
                                                    tr("Images (*.png *.xpm *.jpg)"));

    bildSpieler1.load(fileName);
}
