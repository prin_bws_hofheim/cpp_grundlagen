#include "spieldameablaufsteuerung.h"
#include "spieldamegui.h" // wegen Zugriff auf GUI ("Rueckwaerts)

/**
 * @brief Konstruktor, Festlegen der Grundeinstellungen
 * @param parentIn Zeiger auf GUI
 *
 */
SpielDameAblaufsteuerung::SpielDameAblaufsteuerung(SpielDameGui* parentIn)
{
    parent = parentIn;
    aktuellerZugStart.reihe = -1;
    aktuellerZugStart.spalte = -1;
    aktuellerZugStart.aktiverZug = false;
}

/**
 * @brief standardkonstruktior nicht in Verwendung (Klasse benoetigt Zeiger auf GUI!)
 */
SpielDameAblaufsteuerung::SpielDameAblaufsteuerung()
{

}

/**
 * @brief Destuktor(noch leer)
 */
SpielDameAblaufsteuerung::~SpielDameAblaufsteuerung()
{

}

/**
 * @brief Spielstand aufraumen
 */
void SpielDameAblaufsteuerung::spielNeustarten()
{
    datenhalde.spielNeustarten();
}

/**
 * @brief Liste der Spielsteine von der Halde abholen
 * @return Liste der Steine
 *
 * Die Methode fragt die Liste bei der Datenhalde ab und reicht sie an die GUI weiter.
 * GUI hat keinen direkten Zugriff auf die Datenhalde
 */
vector<SpielDameDatenhalde::spielfeldEintrag> SpielDameAblaufsteuerung::steineListeHolen()
{
    return datenhalde.steineListeHolen();
}

/**
 * @brief Spielablauf - Kontrolle
 * @param reihe aktuelle Reihe
 * @param spalte aktuelle Spalte
 *
 * Die Methode prueft, ob es der erste Klick ist -> Ort speichern, Marker setzen.
 * Wenn es der zweite Klick ist pruefen, ob Zug gueltig (mit weiteren Methoden)
 */
void SpielDameAblaufsteuerung::klickAufFeldBearbeiten(int reihe, int spalte)
{
    SpielDameDatenhalde::spielerTyp spielertyp = SpielDameDatenhalde::spielertypKein;

    spielertyp = datenhalde.getAktiverSpieler();
    if(false == aktuellerZugStart.aktiverZug)
    {
        //        spielertyp = datenhalde.getAktiverSpieler();
        if(getSpieler(reihe, spalte) == spielertyp)
        {
            aktuellerZugStart.reihe = reihe;
            aktuellerZugStart.spalte = spalte;
            aktuellerZugStart.aktiverZug = true;
            qDebug() << "Stein erkannt Reihe: " << aktuellerZugStart.reihe << " Spalte: " << aktuellerZugStart.spalte << datenhalde.getAktiverSpieler();
        }
    }
    else
    {
        if(false == checkSpielfeldBelegt(reihe, spalte))
        {
            if(0 != (reihe + spalte )%2) // Summe ungerade?
                switch(spielertyp)
                {
                case SpielDameDatenhalde::spielerTypSpieler1:
                    if((reihe - 1) == aktuellerZugStart.reihe)
                    {
                        if(((spalte - 1) == aktuellerZugStart.spalte) || ((spalte + 1) == aktuellerZugStart.spalte))
                        {
                            zugAusfuehren(reihe, spalte);
                        }
                    }
                    else
                    {
                        regelSteinSchlagen(reihe, spalte, spielertyp);
                    }
                    break;

                case SpielDameDatenhalde::spielerTypSpieler2:
                    if((reihe + 1) == aktuellerZugStart.reihe)
                    {
                        if(((spalte - 1) == aktuellerZugStart.spalte) || ((spalte + 1) == aktuellerZugStart.spalte))
                        {
                            zugAusfuehren(reihe, spalte);
                        }
                    }
                    else
                    {
                        regelSteinSchlagen(reihe, spalte, spielertyp);
                    }
                    break;

                default: // Zug löschen, Spieler bleibt
                    aktuellerZugStart.reihe = -1;
                    aktuellerZugStart.spalte = -1;
                    aktuellerZugStart.aktiverZug = false;
                }
        }
        else // Zug löschen, Spieler bleibt
        {
            aktuellerZugStart.reihe = -1;
            aktuellerZugStart.spalte = -1;
            aktuellerZugStart.aktiverZug = false;
        }
    }
}

/**
 * @brief Stein setzen, Spieler wechseln, Brett malen
 * @param reihe aktuelle Reihe
 * @param spalte aktuelle Spalte
 *
 * Die Methode laesst den Stein versetzen (achtung beim Schlagen wird der geschlagenen Stein woanders geloescht")
 * Anschliessend wird der aktuelle Spieler korrigiert und alles auf "Anfang" gesetzt (aktiverZug  -> false)
 * zum Schluss wird das Brett neu gezeichet (in der GUI)
 */
void SpielDameAblaufsteuerung::zugAusfuehren(int reihe, int spalte)
{
    datenhalde.steinVersetzen(aktuellerZugStart.reihe, aktuellerZugStart.spalte, reihe, spalte);
    datenhalde.aktiverSpielerWechseln();
    aktuellerZugStart.aktiverZug = false;
    parent->brettNeuZeichnen();
}

/**
 * @brief fragt den aktuellen Spieler in der Halde ab (fuer GUI)
 * @param reihe aktuelle Reihe
 * @param spalte aktuelle Spalte
 * @return
 */
SpielDameDatenhalde::spielerTyp SpielDameAblaufsteuerung::getSpieler(int reihe, int spalte)
{
    return datenhalde.getSpielfeldInhalt(reihe, spalte);
}

/**
 * @brief Feld belegt?
 * @param reihe aktuelle Reihe
 * @param spalte aktuelle Spalte
 * @return Feld belegt ja / nein)
 *
 * prueft, ob auf dem Feld ein Stein (Spieler egal) steht.
 */
bool SpielDameAblaufsteuerung::checkSpielfeldBelegt(int reihe, int spalte)
{
    SpielDameDatenhalde::spielfeldEintrag spielfeld;
    spielfeld.spieler = datenhalde.getSpielfeldInhalt(reihe, spalte);
    if(SpielDameDatenhalde::spielertypKein == spielfeld.spieler)
        return false;

    return true;
}

/**
 * @brief Umwandeln vom spielstein zu Dame
 * @return ja/nein?
 * @todo noch implemetieren!
 *
 */
bool SpielDameAblaufsteuerung::regelSteinZuDame()
{

}

/**
 * @brief wenn zu schlagen, dann tu's
 * @param reihe aktuelle Reihe
 * @param spalte aktuelle Spalte
 * @param spielertyp Spieler 1 oder 2
 *
 * Prueft, ob ein Stein zu Schlagen ist. Wenn moeglich, dann wird der Stein geschlagen und entfernt.
 */
void SpielDameAblaufsteuerung::regelSteinSchlagen(int reihe, int spalte, SpielDameDatenhalde::spielerTyp spielertyp)
{
    switch(spielertyp)
    {
    case SpielDameDatenhalde::spielerTypSpieler1:
        if((reihe - 2) == aktuellerZugStart.reihe)
        {
            if(((spalte - 2) == aktuellerZugStart.spalte) || ((spalte + 2) == aktuellerZugStart.spalte))
            {
                if((spalte - 2) == aktuellerZugStart.spalte)
                    datenhalde.steinEntfernen(reihe - 1, spalte - 1);
                else
                    datenhalde.steinEntfernen(reihe - 1, spalte + 1);

                zugAusfuehren(reihe, spalte);
            }
            else
            {
                zugDame();
            }
        }
        else
            zugDame();
        break;

    case SpielDameDatenhalde::spielerTypSpieler2:
        if((reihe + 2) == aktuellerZugStart.reihe)
        {
            if(((spalte - 2) == aktuellerZugStart.spalte) || ((spalte + 2) == aktuellerZugStart.spalte))
            {
                if((spalte - 2) == aktuellerZugStart.spalte)
                    datenhalde.steinEntfernen(reihe + 1, spalte - 1);
                else
                    datenhalde.steinEntfernen(reihe + 1, spalte + 1);

                zugAusfuehren(reihe, spalte);
            }
            else
            {
                zugDame();
            }
        }
        else
            zugDame();

        break;

    default: // Zug löschen, Spieler bleibt
        aktuellerZugStart.reihe = -1;
        aktuellerZugStart.spalte = -1;
        aktuellerZugStart.aktiverZug = false;
    }

}

void SpielDameAblaufsteuerung::zugDame()
{

}
