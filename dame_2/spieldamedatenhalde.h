#ifndef SPIELDAMEDATENHALDE_H
#define SPIELDAMEDATENHALDE_H

#include <vector>
using namespace std;

class SpielDameDatenhalde
{

public:
    SpielDameDatenhalde();

    enum spielerTyp {spielertypKein = -1, spielerTypSpieler1, spielerTypSpieler2} ;
    enum spielSteinTyp {spielsteinKein = -1, spielsteinTypStein, spielsteinTypDame};

    struct spielfeldBelegung
    {
//        int reihe;
//        int spalte;
        spielerTyp spieler;
        spielSteinTyp spielstein;
    };

    struct spielfeldEintrag
    {
        int reihe;
        int spalte;
        spielerTyp spieler;
        spielSteinTyp spielstein;
    };


    // Breite == Hoehe
    static const int feldBreiteGui = 100;

    void spielNeustarten();
    vector<spielfeldEintrag> steineListeHolen();
    spielerTyp getAktiverSpieler();
    void aktiverSpielerWechseln();
    spielerTyp getSpielfeldInhalt(int reihe, int spalte);
    void steinVersetzen(int reiheVorher, int spalteVorher, int reiheNachher, int spalteNachher);
    void steinEntfernen(int reiheVorher, int spalteVorher);

private:
    spielerTyp aktiverSpieler;
    spielfeldBelegung spielfeld[8][8];//Reihe, Spalte


};

#endif // SPIELDAMEDATENHALDE_H
