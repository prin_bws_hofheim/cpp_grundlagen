var indexSectionsWithContent =
{
  0: "abcdfgkmoprsuz~",
  1: "ks",
  2: "u",
  3: "ms",
  4: "abcgkmoprsz~",
  5: "abdfprsu",
  6: "s",
  7: "s",
  8: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "enums",
  7: "enumvalues",
  8: "pages"
};

var indexSectionLabels =
{
  0: "Alle",
  1: "Klassen",
  2: "Namensbereiche",
  3: "Dateien",
  4: "Funktionen",
  5: "Variablen",
  6: "Aufzählungen",
  7: "Aufzählungswerte",
  8: "Seiten"
};

