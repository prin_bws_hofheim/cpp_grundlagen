var menudata={children:[
{text:"Hauptseite",url:"index.html"},
{text:"Zusätzliche Informationen",url:"pages.html"},
{text:"Namensbereiche",url:"namespaces.html",children:[
{text:"Liste aller Namensbereiche",url:"namespaces.html"}]},
{text:"Klassen",url:"annotated.html",children:[
{text:"Auflistung der Klassen",url:"annotated.html"},
{text:"Klassen-Verzeichnis",url:"classes.html"},
{text:"Klassenhierarchie",url:"inherits.html"},
{text:"Klassen-Elemente",url:"functions.html",children:[
{text:"Alle",url:"functions.html",children:[
{text:"a",url:"functions.html#index_a"},
{text:"b",url:"functions.html#index_b"},
{text:"c",url:"functions.html#index_c"},
{text:"d",url:"functions.html#index_d"},
{text:"f",url:"functions.html#index_f"},
{text:"g",url:"functions.html#index_g"},
{text:"k",url:"functions.html#index_k"},
{text:"m",url:"functions.html#index_m"},
{text:"o",url:"functions.html#index_o"},
{text:"p",url:"functions.html#index_p"},
{text:"r",url:"functions.html#index_r"},
{text:"s",url:"functions.html#index_s"},
{text:"u",url:"functions.html#index_u"},
{text:"z",url:"functions.html#index_z"},
{text:"~",url:"functions.html#index_0x7e"}]},
{text:"Funktionen",url:"functions_func.html"},
{text:"Variablen",url:"functions_vars.html"},
{text:"Aufzählungen",url:"functions_enum.html"},
{text:"Aufzählungswerte",url:"functions_eval.html"}]}]},
{text:"Dateien",url:"files.html",children:[
{text:"Auflistung der Dateien",url:"files.html"},
{text:"Datei-Elemente",url:"globals.html",children:[
{text:"Alle",url:"globals.html"},
{text:"Funktionen",url:"globals_func.html"}]}]}]}
