#ifndef SPIELDAMEGUI_H
#define SPIELDAMEGUI_H

#include <QWidget>
#include <QPaintEvent>

#include "spieldameablaufsteuerung.h"
#include "spieldamedatenhalde.h"

#include <QPainter>
#include <QDebug>
#include <QPushButton>
#include <QLabel>
#include <QFileDialog>

namespace Ui {
class SpielDameGui;
}

class SpielDameGui : public QWidget
{
    Q_OBJECT

public:
    explicit SpielDameGui(QWidget *parent = 0);
    ~SpielDameGui();

    struct spielfeldBelegungGui
    {
        int reihe;
        int spalte;
        QLabel* spielfeldLabel;
    };

    void brettNeuZeichnen();

public slots:
    void spielNeustarten();

private slots:
    void on_btnLoadImagePlayer1_clicked();
    void on_btnLoadImagePlayer2_clicked();

private:
    Ui::SpielDameGui *ui;

    void paintEvent(QPaintEvent *event);
    void mousePressEvent(QMouseEvent *event);

    SpielDameAblaufsteuerung *ablaufsteuerung;

    vector<spielfeldBelegungGui> spielfeldBelegung;

    QPixmap bildSpieler1;
    QPixmap bildSpieler2;
};

#endif // SPIELDAMEGUI_H
