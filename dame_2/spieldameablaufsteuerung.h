#ifndef SPIELDAMEABLAUFSTEUERUNG_H
#define SPIELDAMEABLAUFSTEUERUNG_H

#include "spieldamedatenhalde.h"
#include <QDebug>

class SpielDameGui; //dummy statt "Ring-Include"

class SpielDameAblaufsteuerung
{
public:
    SpielDameAblaufsteuerung(SpielDameGui *parentIn);
    ~SpielDameAblaufsteuerung();

    struct klickPos{
        int reihe;
        int spalte;
        bool aktiverZug;
    };

    void klickAufFeldBearbeiten(int reihe, int spalte);
    void spielNeustarten();
    vector<SpielDameDatenhalde::spielfeldEintrag> steineListeHolen();

private:
    SpielDameAblaufsteuerung(); //dummy Konstuktor, soll nicht benutzt werden!
    bool checkSpielfeldBelegt(int reihe, int spalte);
    bool regelSteinZuDame();
    void regelSteinSchlagen(int reihe, int spalte, SpielDameDatenhalde::spielerTyp spielertyp);
    void zugAusfuehren(int reihe, int spalte);
    void zugDame();
    SpielDameDatenhalde::spielerTyp getSpieler(int reihe, int spalte);

    SpielDameDatenhalde datenhalde;
    SpielDameGui* parent;

    klickPos aktuellerZugStart;
};

#endif // SPIELDAMEABLAUFSTEUERUNG_H
