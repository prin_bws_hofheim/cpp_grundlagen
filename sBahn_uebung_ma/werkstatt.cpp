#include "werkstatt.h"

Werkstatt::Werkstatt()
{

}

Sitz* Werkstatt::getSitz(int sitzTyp)
{
    Sitz* tempSitz = nullptr;
    for(int i = 0; i < sitzeWerkstatt.size(); i++)
        if(sitzeWerkstatt.at(i)->getKlasse() == sitzTyp)
        {
            tempSitz = sitzeWerkstatt.at(i);
            sitzeWerkstatt.erase(sitzeWerkstatt.begin() + i);
        }
    return tempSitz;
}

void Werkstatt::setSitz(Sitz *sitzNew)
{
    sitzeWerkstatt.push_back(sitzNew);
}

void Werkstatt::erzeugeSitz(int klasse)
{
    Sitz* tempSitz = nullptr;

    tempSitz = new Sitz(klasse);
    sitzeWerkstatt.push_back(tempSitz);
}

Motor* Werkstatt::getMotor()
{
    Motor* tempMotor = nullptr;
    if(!motorenWerkstatt.size())
        return tempMotor;

    tempMotor = motorenWerkstatt.back();
    motorenWerkstatt.pop_back();

    return tempMotor;
}

void Werkstatt::setMotor(Motor *motorNew)
{
    motorenWerkstatt.push_back(motorNew);
}

void Werkstatt::erzeugeMotor()
{
    Motor* tempMotor = nullptr;

    tempMotor = new Motor(375, 400); // Einheitsmotor
    motorenWerkstatt.push_back(tempMotor);
}

bool Werkstatt::reinigeSitz(int sitzNr)
{
    Sitz* tempSitz = nullptr;

    if(sitzeWerkstatt.size() < sitzNr)
        return false;

    tempSitz = sitzeWerkstatt.at(sitzNr);

    if(nullptr == tempSitz)
        return false; // doch kein Objekt, Finger weg!

    if(1<tempSitz->getZustand() && 6 < tempSitz->getZustand())
    {
        tempSitz->setZustand(2);
        return true;
    }

    return false;
}

void Werkstatt::verschrotteSitz(int sitzNr)
{
    Sitz* tempSitz = nullptr;
    if(sitzeWerkstatt.size() < sitzNr)
        return;

    tempSitz = sitzeWerkstatt.at(sitzNr);

    if(nullptr == tempSitz)
        return; // doch kein Objekt, Finger weg!

    sitzeWerkstatt.erase(sitzeWerkstatt.begin() + sitzNr);
    delete tempSitz;
    tempSitz = nullptr;
}
