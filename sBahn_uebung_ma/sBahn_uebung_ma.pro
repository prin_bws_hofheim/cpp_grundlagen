TEMPLATE = app
CONFIG += console c++11
CONFIG -= app_bundle
CONFIG -= qt

SOURCES += \
        main.cpp \
        motor.cpp \
        sitz.cpp \
        triebwagen.cpp \
        werkstatt.cpp

HEADERS += \
    motor.h \
    sitz.h \
    triebwagen.h \
    werkstatt.h
