#include "motor.h"

Motor::Motor()
{
    leistungKw = 3;
    spannungV = 400;
}

int Motor::getSpannungV() const
{
    return spannungV;
}

int Motor::getLeistungKw() const
{
    return leistungKw;
}

Motor::Motor(int newLeistungKw, int newSpannungV)
{
    leistungKw = newLeistungKw;
    spannungV = newSpannungV;
}

void Motor::drehe(int anzahlUmdrehungen)
{
    cout << "drehe: " << anzahlUmdrehungen << " mal" << endl;
}
