# Übungsaufgabe zu S­Bahn / Übung zu Klassen (Teil 2)

Im zweiten Teil soll das Erstellen (und verschrotten / Zerstören) der Sitze und Motoren in eine 
Fachwerkstatt übertragen werden.
Lege dir eine Kopie des Projekts an oder (besser) speichere es in einem 
Versionskontrollsystem (z.B. git)

## Aufgaben

 1. Lege eine neue Klasse Werkstatt an.
 2. Werkstatt hat ein Attribut Sitz* sitzeWerkstatt[anzahl] und ein Attribut Motor* 
motorenWerkstatt[anzahl]. (Anzahl = 200);
 3. Erstelle Methoden, um die Sitze und Motoren zu erstellen (in dem entsprechenden Array 
ablegen). Die Arrays sind hier Regale für die Teile.*void erzeugeSitz(int klasse);    void 
erzeugeMotor();
 4. Erstelle Methoden, um einen Sitz (oder einen Motor) aus dem Array ("Regal") zu 
entnehmen und zurückzugeben.
 5. Erstelle Methoden, um einen Sitz (oder einen Motor) in das Array ("Regal") abzulegen.
 6. Erstelle eine Methode um Sitze zu reinigen (bool reinigeSitz(int sitzNr);). *Bedingung: Stufe 
1 und Stufe 6 wird nicht gereinigt. Nach der Reinigung hat der Sitz Stufe 2. Die methode soll 
(bool) zurückmelden, ob das Reinigen erfolgreich (möglich) war.
 7. Erstelle eine Methode verschrotteSitz(), die den Sitz aus dem Regal nimmt und mit delete 
entsorgt (delete ist die C++ Version der Müllpresse).
 8. Ändere die main­Funktion (oder die GUI), sodass man in der Werkstatt Sitze und Motoren 
anlegen (und verschrotten) kann. Weiter soll es möglich sein Sitze (und Motoren) als Objekt 
aus dem Bestand der Werkstatt in ein Fahrzeug (Triebwagen) zu versetzen. Beim Versetzen 
soll keine neue Kopie erzeugt werden, sondern dasselbe Objekt verschoben werden!
