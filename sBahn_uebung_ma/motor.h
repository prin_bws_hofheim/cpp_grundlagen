#ifndef MOTOR_H
#define MOTOR_H

#include <iostream>
using namespace std;

/**
 * @brief Elektromotor
 *
 * will Strom haben, zum drehen / fahren
 */
class Motor
{
public:
    Motor(int newLeistungKw, int newSpannungV);
    void drehe(int anzahlUmdrehungen);

    int getLeistungKw() const;

    int getSpannungV() const;

private:
    Motor();
    int leistungKw;
    int spannungV;
};

#endif // MOTOR_H
