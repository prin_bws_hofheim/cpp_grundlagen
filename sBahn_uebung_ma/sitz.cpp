#include "sitz.h"

Sitz::Sitz()
{
    klasse = 0;
    zustand = 0;
}

/**
 * @brief Neuer Sitz für eine Klasse
 * @param klasseNew 0 = Führerstand, 1 = 1. 2 = 2. 3+ gibts nicht
 *
 * Die Klasse muss beim Anlegen angegeben werden. Die Arten können nachträglich nicht verändert werden.
 */
Sitz::Sitz(int newKlasse)
{
    klasse = newKlasse;
    zustand = 0;
}

int Sitz::getKlasse()
{
    return klasse;
}

int Sitz::getZustand()
{
    return zustand;
}

void Sitz::setZustand(int newZustand)
{
    zustand = newZustand;
}
