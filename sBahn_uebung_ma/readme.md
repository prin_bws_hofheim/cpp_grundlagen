# Übung zu Klassen

In der Übung geht es darum Objekte von Klassen anzulegen.

Die Klasse Motor und Klasse Sitz haben Attribute. Bestimmte Attribute müssen beim Erzeugen des Objekts festgelegt werden.
Z.B. muss bei einem Sitz festgelegt werden, ob es ein Sitz für den Führerstand (0), ein Sitz erste Klasse (1) oder zweite Klasse (2) sein soll. 

Der Konstruktor bei Motor benötigt die (maximale) Leistung und die Spannung.
Bei Motor ist der default-Konstruktor private. Somit müssen beim Anlegen die Parameter (Leistung und Spannung) angegeben werden.

## Aufgaben

1 lege im Triebwagen ein Objekt vom Motor an und lege dort die Leitung auf 375 und Spannung auf 400 fest. Verändere nicht den Quelltext von Motor.

2 Lege in Triebwagen 178 Objekte von Sitz an (Objekt 0 und 177 sind Typ 0, 16 Stück (1 - 16) sind Typ 1, Rest ist Typ 2.

3 Das Fahrzeug soll fahren. 0 Umdrehungen bedeutet Fahrzeug steht. 1400 Umdrehungen = vMax (140 km/h) Erstelle eine Methode in Triebfahrzeug void beschleunigen(int kmProH), die die Geschwindigkeit um kmProH erhöht (bis maximal vMax)

4 Erstelle eine Methode bremsen(int kmProH) zum Abbremsen (0 = minimale Geschwindigkeit)

5 Die Motoren sollen die korrekte AnzahlUmdrehungen (pro Minute) haben, ergänze das Attribut uMin und eine Methode, die den Wert zurückgibt.

6 in der Funktion main soll die Ein- und Ausgabe stattfinden. Ergänze Triebwagen, dass es möglich ist 

  * Die Leistung aller Motoren, 

  * die aktuelle uMin aller Motoren
  
  * den Zustand aller Sitze
  
  Ausgeben zu lassen.
