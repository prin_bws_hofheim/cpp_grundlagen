#ifndef WERKSTATT_H
#define WERKSTATT_H

#include <vector>
#include "sitz.h"
#include "motor.h"

using namespace std;

class Werkstatt
{
public:
    Werkstatt();

    void erzeugeSitz(int klasse);
    void erzeugeMotor();
    bool reinigeSitz(int sitzNr);
    void verschrotteSitz(int sitzNr);

    Sitz *getSitz(int sitzTyp);
    void setSitz(Sitz* sitzNew);
    Motor *getMotor();
    void setMotor(Motor *motorNew);

private:
    vector<Sitz*> sitzeWerkstatt;
    vector<Motor*> motorenWerkstatt;
};

#endif // WERKSTATT_H
