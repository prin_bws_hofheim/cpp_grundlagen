#ifndef TRIEBWAGEN_H
#define TRIEBWAGEN_H

#include <string>
#include "sitz.h"
#include "motor.h"
using namespace std;

/**
 * @brief Triebwagen.
 *
 * Ein Tiebwagen Besteht aus Motor (8), Sitze (176 + 2)
 *
 * Vmax=140 km/h
 */
class Triebwagen
{
public:
    Triebwagen();
    ~Triebwagen();
    static const int anzSitze = 178;
    static const int anzMotoren = 8;

    int getInfoSitzKlasse(int id);
    int getInfoSitzZustand(int id);

    int getInfoLeistungMotor(int id);
    int getInfoSpannungMotor(int id);

    int getVMax();
    int getVAkt();

    bool sitzEinbauen(Sitz *sitzNew, int id);
    Sitz *sitzAusbauen(int id);

    bool motorEinbauen(Motor *motorNew, int id);
    Motor *motorAusbauen(int id);

    int getLinie() const;
    void setLinie(int value);

    string getAktHaltestelle() const;
    void setAktHaltestelle(const string &value);

private:
    Motor *fahrmotoren[anzMotoren];
    Sitz *sitze[anzSitze];

    int vMax;
    int vAkt;
    int linie;
    string aktHaltestelle;
};

#endif // TRIEBWAGEN_H
