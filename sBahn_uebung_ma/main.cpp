#include <iostream>
#include "triebwagen.h"
#include "werkstatt.h"

using namespace std;

void menuWerkstatt(int auswahl, Werkstatt *myWerkstatt, Triebwagen *flotteSBahn[5])
{
    int pos = 0;
    Sitz *tempSitz = nullptr;

    switch(auswahl)
    {
    case 1:
        cout << "Neuen Zug kaufen. Welche Position (0-4)?" << endl << "-1 = Abbruch" << endl;
        cin>> pos;

        if(0<= pos && 4 >= pos)
            if(!flotteSBahn[pos])
            {
                flotteSBahn[pos] = new Triebwagen();
                myWerkstatt->erzeugeSitz(0);
                myWerkstatt->erzeugeSitz(0);
                for(int i = 0; i < 16; i++)
                    myWerkstatt->erzeugeSitz(1);
                for(int i = 0; i < 160; i++)
                    myWerkstatt->erzeugeSitz(1);

                tempSitz = myWerkstatt->getSitz(0);
                flotteSBahn[pos]->sitzEinbauen(tempSitz, 0);
                tempSitz = myWerkstatt->getSitz(0);
                flotteSBahn[pos]->sitzEinbauen(tempSitz, 177);
                for(int i = 1; i < 16; i++)
                {
                    tempSitz = myWerkstatt->getSitz(1);
                    flotteSBahn[pos]->sitzEinbauen(tempSitz, i);
                }
            }
        break;
    case 2:
        break;

    case 3:
        break;

    case 99:
        cout << "Zug verschrotten. Welche Position (0-4)?" << endl << "-1 = Abbruch" << endl;
        cin>> pos;

        if(0<= pos && 4 >= pos)
            if(!flotteSBahn[pos])
                flotteSBahn[pos] = new Triebwagen();
        break;
    }
}

int main()
{
    const int anzZuege = 5;
    Triebwagen *flotteSBahn[anzZuege] = {nullptr, nullptr, nullptr, nullptr, nullptr};
    Werkstatt *myWerkstatt = new Werkstatt;
    int eingabe = -1;

    cout << "S-Bahn-Flotte" << endl;

    while(eingabe)
    {
        cout << "Menu:" << endl
             << "1 neuen Zug kaufen" << endl
             << "2 Zug fahren" << endl
             << "3 Anzeige Flotte" << endl
             << "4 Anzeige Zustand Wagen" << endl
             << "0 Programm Ende" << endl
             << "99 Zug verschrotten" << endl << endl;
        cin >> eingabe;

        if(1 == eingabe||99 == eingabe)
            menuWerkstatt(eingabe, myWerkstatt, flotteSBahn);
        else
            switch(eingabe)
            {
            case 2:
                cout << "Wohin soll die Fahrt gehen?" << endl;
                cout << "1 Frankfurt HBf" << endl << "2 Hanau HBf" << endl << "3 Wiesbaden HBf";
                cin>> eingabe;
                switch (eingabe)
                {
                case 1:
                case 2:
                    cout << "Fahre Sie zum ziel, bitte anschnallen. Wir fahren ab" << endl;
                    break;
                case 3:
                    cout << "Schon die Salzbachtalbruecke kennen gelernt? " << endl << "Wir haben Ersatzbusverkehr.";
                    return -1;

                }
                break;

            case 3:
                cout << "Unsere Flotte:" << endl;
                for(int i = 0; i < anzZuege; i++)
                {
                    cout << "Position " << i << ": ";
                    if(flotteSBahn[i])
                    {
                        cout << flotteSBahn[i]->getAktHaltestelle();
                        cout << "aktuelle Geschwindigkeit: ";
                        cout << flotteSBahn[i]->getVAkt();
                    }
                    else
                        cout << "Ihr freundlicher Hersteller liefert ihn schnellst moeglich nach Zahlungseingang."
                             << endl << "Bitte bestellen und zahlen.";
                    cout  << endl << endl;
                }
                break;
            case 4:cout << "Unsere Flotte:" << endl;
                for(int i = 0; i < anzZuege; i++)
                {
                    if(flotteSBahn[i])
                    {
                        for(int j = 0; j < Triebwagen::anzSitze; j++)
                            cout << "Sitz " << j << ": " << flotteSBahn[i]->getInfoSitzZustand(j) << endl;
                    }
                    cout  << endl << endl;
                }
                break;
            case 0:
                break;
            default:
                cout << "Ham mer net" << endl;
                break;
            }
    }

    delete myWerkstatt;
    for(int i = 0; i <5; i++)
        if(flotteSBahn[i])
        {
            delete flotteSBahn[i];
            flotteSBahn[i] = nullptr;
            return 0;
        }
}
