#include "triebwagen.h"

Triebwagen::Triebwagen()
{
    for(int i = 0; i < anzMotoren; i++)
        fahrmotoren[i] = nullptr;

    for(int i = 0; i < anzSitze; i++)
        sitze[i] = nullptr;

    vMax = 140;
    vAkt = 0;
    linie = 8;
    aktHaltestelle = "Wiesbaden Ost";
}

Triebwagen::~Triebwagen()
{
    for(int i = 0; i < anzMotoren; i++)
    {
        delete fahrmotoren[i];
        fahrmotoren[i] = nullptr;

    }
    for(int i = 0; i < anzSitze; i++)
    {
        delete sitze[i];
        sitze[i] = nullptr;
    }
}

int Triebwagen::getInfoSitzKlasse(int id)
{
    if(0< id && anzSitze > id)
        if(sitze[id])
        return sitze[id]->getKlasse();

    return -1;
}

int Triebwagen::getInfoSitzZustand(int id)
{
    if(0< id && anzSitze > id)
        if(sitze[id])
        return sitze[id]->getZustand();
    return -1;
}

int Triebwagen::getInfoLeistungMotor(int id)
{
    if(0< id && anzMotoren > id)
        if(fahrmotoren[id])
        return fahrmotoren[id]->getLeistungKw();
    return 0;
}

int Triebwagen::getInfoSpannungMotor(int id)
{
    if(0< id && anzMotoren > id)
        if(fahrmotoren[id])
        return fahrmotoren[id]->getSpannungV();
    return 0;
}

int Triebwagen::getVMax()
{
    return vMax;
}

int Triebwagen::getVAkt()
{
    return vAkt;
}

bool Triebwagen::sitzEinbauen(Sitz *sitzNew, int id)
{
    if(nullptr == sitze[id])
    {
        sitze[id] = sitzNew;
        return true;
    }
    return false;
}

Sitz *Triebwagen::sitzAusbauen(int id)
{
    Sitz* tempSitz = nullptr;

    if(anzSitze > id)
    {
        tempSitz = sitze[id];
        sitze[id] = nullptr;
    }
    return tempSitz;
}

bool Triebwagen::motorEinbauen(Motor *motorNew, int id)
{
    if(nullptr == fahrmotoren[id])
    {
        fahrmotoren[id] = motorNew;
        return true;
    }
    return false;
}

Motor *Triebwagen::motorAusbauen(int id)
{
    Motor* tempMotor = nullptr;

    if(anzMotoren > id)
    {
        tempMotor = fahrmotoren[id];
        sitze[id] = nullptr;
    }
    return tempMotor;
}

int Triebwagen::getLinie() const
{
    return linie;
}

void Triebwagen::setLinie(int value)
{
    linie = value;
}

string Triebwagen::getAktHaltestelle() const
{
    return aktHaltestelle;
}

void Triebwagen::setAktHaltestelle(const string &value)
{
    aktHaltestelle = value;
}
