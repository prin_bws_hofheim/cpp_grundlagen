#ifndef SITZ_H
#define SITZ_H


class Sitz
{
public:
    Sitz();
    Sitz(int newKlasse);

    int getKlasse();

    int getZustand();
    void setZustand(int newZustand);

private:
    int klasse; // 0 = Fuehrerstand
    int zustand; // 0 = neu, 1 = leicht gebraucht, ... 6 = schrottreif
};

#endif // SITZ_H
