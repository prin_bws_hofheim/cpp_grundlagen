#ifndef EVENT_H
#define EVENT_H

#include <QString>
#include <QDate>


class Event
{
public:
    Event();

    int getId();
    static int getNextId();
private:
    static int nextId;
    QString name;
    QDate datum;
    int id;

};

#endif // EVENT_H
