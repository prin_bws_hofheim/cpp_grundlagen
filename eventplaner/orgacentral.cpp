#include "orgacentral.h"

OrgaCentral::OrgaCentral()
{

}

void OrgaCentral::neuerTeilnehmer(Teilnehmer *neuTeilnehmer)
{
    meineTeilnehmerListe.push_back(neuTeilnehmer);
}

Teilnehmer *OrgaCentral::teilnehmerSuchen(QString suchVorname)
{
    for(unsigned short i = 0; i < meineTeilnehmerListe.size(); i++)
    {
        if(!meineTeilnehmerListe[i]->getVorname().compare(suchVorname))
        {
            return meineTeilnehmerListe[i];
        }
    }
    return nullptr;
}

void OrgaCentral::teilnehmerAendern(Teilnehmer *teilnehmerAnders)
{
    for(unsigned short i = 0; i < meineTeilnehmerListe.size(); i++)
    {
        if(meineTeilnehmerListe[i] == teilnehmerAnders)
        {
            meineTeilnehmerListe[i] = teilnehmerAnders;
        }
    }
}

void OrgaCentral::teilnehmerAendern(QString altVorname, QString neuVorname, QString neuNachname, QString neuTel, QString neuMail)
{
    for(unsigned short i = 0; i < meineTeilnehmerListe.size(); i++)
    {
        if(!meineTeilnehmerListe[i]->getVorname().compare(altVorname))
        {
            meineTeilnehmerListe[i]->setVorname(neuVorname);
            meineTeilnehmerListe[i]->setNachname(neuNachname);
            meineTeilnehmerListe[i]->setTel(neuTel);
            meineTeilnehmerListe[i]->setMail(neuMail);
        }
    }
}

void OrgaCentral::eventNeu()
{
    Event * tempEvent = nullptr;
    tempEvent = new Event;

    qDebug() << " event: " << tempEvent->getId();
}
