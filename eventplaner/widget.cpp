#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget)
{
    ui->setupUi(this);
    myOrgaCentral = new OrgaCentral;
}

Widget::~Widget()
{
    delete ui;
}

void Widget::on_btnNeuerTeilnehmer_clicked()
{
    Teilnehmer *tempTeilnehmer = nullptr;
//    Teilnehmer *tempTeilnehmer2 = nullptr;

    tempTeilnehmer = new Teilnehmer;
    tempTeilnehmer->setVorname("Julian");
    tempTeilnehmer->setNachname(ui->edtNachname->text());

//    tempTeilnehmer2 = tempTeilnehmer;
    myOrgaCentral->neuerTeilnehmer(tempTeilnehmer);
    qDebug() << "Teilnehmer: " << tempTeilnehmer->getId();
}

void Widget::on_btnSucheTeilnehmer_clicked()
{
    QString suchVorname;
    Teilnehmer *tempTeilnehmer = nullptr;

    suchVorname = ui->edtVorname->text();
    tempTeilnehmer = myOrgaCentral->teilnehmerSuchen(suchVorname);
    ui->edtVorname->setText(tempTeilnehmer->getVorname());
    ui->edtNachname->setText(tempTeilnehmer->getNachname());
    ui->edtTel->setText(tempTeilnehmer->getTel());
    ui->edtMail->setText(tempTeilnehmer->getMail());
}

void Widget::on_btnEventNeu_clicked()
{
    myOrgaCentral->eventNeu();
}
