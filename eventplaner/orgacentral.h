#ifndef ORGACENTRAL_H
#define ORGACENTRAL_H
#include <QDebug>
#include "teilnehmer.h"
#include "event.h"
using namespace std;

class OrgaCentral
{
public:
    OrgaCentral();
    void neuerTeilnehmer(Teilnehmer * neuTeilnehmer);
    Teilnehmer * teilnehmerSuchen(QString suchVorname);
    void teilnehmerAendern(Teilnehmer *teilnehmerAnders);
    void teilnehmerAendern(QString altVorname, QString neuVorname, QString neuNachname, QString neuTel, QString neuMail);
    void eventNeu();

private:
    vector<Teilnehmer *> meineTeilnehmerListe;
    vector<Event *> events;
};

#endif // ORGACENTRAL_H
