#ifndef TEILNEHMER_H
#define TEILNEHMER_H

#include <QString>

class Teilnehmer
{
public:
    Teilnehmer();

    QString getVorname() const;
    void setVorname(const QString &value);

    QString getNachname() const;
    void setNachname(const QString &value);

    QString getTel() const;
    void setTel(const QString &value);

    QString getMail() const;
    void setMail(const QString &value);

    int getId();
private:
    QString vorname;
    QString nachname;
    QString tel;
    QString mail;
    static int nextId;
    static int getNextId();
    int id;
};

#endif // TEILNEHMER_H
