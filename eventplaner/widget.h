#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QList>
#include "orgacentral.h"

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnNeuerTeilnehmer_clicked();

    void on_btnSucheTeilnehmer_clicked();

    void on_btnEventNeu_clicked();

private:
    Ui::Widget *ui;
    OrgaCentral *myOrgaCentral;
};

#endif // WIDGET_H
