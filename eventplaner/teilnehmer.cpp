#include "teilnehmer.h"

Teilnehmer::Teilnehmer()
{
    id = getNextId();
}

QString Teilnehmer::getVorname() const
{
    return vorname;
}

void Teilnehmer::setVorname(const QString &value)
{
    vorname = value;
}

QString Teilnehmer::getNachname() const
{
    return nachname;
}

void Teilnehmer::setNachname(const QString &value)
{
    nachname = value;
}

QString Teilnehmer::getTel() const
{
    return tel;
}

void Teilnehmer::setTel(const QString &value)
{
    tel = value;
}

QString Teilnehmer::getMail() const
{
    return mail;
}

void Teilnehmer::setMail(const QString &value)
{
    mail = value;
}

int Teilnehmer::getId()
{
    return id;
}

int Teilnehmer::getNextId()
{
    return nextId++;
}
