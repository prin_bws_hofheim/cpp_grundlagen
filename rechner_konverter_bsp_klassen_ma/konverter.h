#ifndef KONVERTER_H
#define KONVERTER_H

#include <string>
#include <QString>
using namespace std;

class Konverter
{
public:
    Konverter();
    int stringToInt(string input);
    double stringToDouble(string input);

    int stringToInt(QString input);
    double stringToDouble(QString input);

};

#endif // KONVERTER_H
