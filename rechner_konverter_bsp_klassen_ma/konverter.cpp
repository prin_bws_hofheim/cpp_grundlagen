#include "konverter.h"

Konverter::Konverter()
{

}

/**
 * @brief  string (std) to int
 * @param input string (std)
 * @return Wert als int (abs)
 *
 * Die Methode konvertiert den Inhalt des Strings nach int. Vorzeichen werden ignoriert.
 *
 */
int Konverter::stringToInt(string input)
{
    int tempInt = 0;
    char tempChar = 0;

    for(unsigned long i =0; i < input.size(); i++)
    {
        tempChar = input[i];

        tempInt *= 10;
        switch (tempChar)
        {
        case '0':
            break;
        case '1':
            tempInt += 1;
            break;
        case '2':
            tempInt += 2;
            break;
        case '3':
            tempInt += 3;
            break;
        case '4':
            tempInt += 4;
            break;
        case '5':
            tempInt += 5;
            break;
        case '6':
            tempInt += 6;
            break;
        case '7':
            tempInt += 7;
            break;
        case '8':
            tempInt += 8;
            break;
        case '9':
            tempInt += 9;
            break;
        default:
            tempInt /= 10;
        }
    }
    return tempInt;
}

double Konverter::stringToDouble(string input)
{
    return 0.0;
}

/**
 * @brief  string (std) to int
 * @param input string (std)
 * @return Wert als int (abs)
 *
 * (Überladung)
 * Die Methode konvertiert den Inhalt des Strings nach int. Vorzeichen werden ignoriert.
 *
 */
int Konverter::stringToInt(QString input)
{
    int tempInt = 0;
    char tempChar = 0;

    for(int i = 0; i < input.size(); i++)
    {
        tempChar = input[i].toLatin1();

        tempInt *= 10;
        switch (tempChar)
        {
        case '0':
            break;
        case '1':
            tempInt += 1;
            break;
        case '2':
            tempInt += 2;
            break;
        case '3':
            tempInt += 3;
            break;
        case '4':
            tempInt += 4;
            break;
        case '5':
            tempInt += 5;
            break;
        case '6':
            tempInt += 6;
            break;
        case '7':
            tempInt += 7;
            break;
        case '8':
            tempInt += 8;
            break;
        case '9':
            tempInt += 9;
            break;
        default:
            tempInt /= 10;
        }
    }
    return tempInt;
}

double Konverter::stringToDouble(QString input)
{
    return input.toDouble();
}
