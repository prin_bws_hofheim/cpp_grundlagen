#-------------------------------------------------
#
# Project created by QtCreator 2017-03-01T10:42:44
#
#-------------------------------------------------

QT       += core gui

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = dateizugriffe_editoren
TEMPLATE = app


SOURCES += main.cpp\
        widget.cpp \
    dateizugriffe_aufg1.cpp \
    dateizugriffe_aufg2.cpp \
    dateien_abstrakte_klasse.cpp

HEADERS  += widget.h \
    dateizugriffe_aufg1.h \
    dateizugriffe_aufg2.h \
    dateien_abstrakte_klasse.h

FORMS    += widget.ui

DISTFILES += \
    lorem_ipsum.txt
