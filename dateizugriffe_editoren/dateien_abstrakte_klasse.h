#ifndef DATEIEN_ABSTRAKTE_KLASSE_H
#define DATEIEN_ABSTRAKTE_KLASSE_H
#include <QString>

class Dateien_abstrakte_klasse
{
public:
    Dateien_abstrakte_klasse();
    virtual ~Dateien_abstrakte_klasse();

    virtual bool loadFile(QString fileName, QString &textToOutput)=0;
};

#endif // DATEIEN_ABSTRAKTE_KLASSE_H
