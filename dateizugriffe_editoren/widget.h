#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QFileDialog>
#include <cmath>

#include "dateizugriffe_aufg1.h"
#include "dateizugriffe_aufg2.h"

#include "dateien_abstrakte_klasse.h"

//using namespace std;

namespace Ui {
class Widget;
}

class Widget : public QWidget
{
    Q_OBJECT

public:
    explicit Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnSave_clicked();
    void on_btnLoad_clicked();

    void on_rbtOneEntry_clicked();
    void on_rbtManyEntrys_clicked();

    void on_btnBack_clicked();

    void on_btnForward_clicked();

    void on_pushButton_clicked();

    void on_pushButton_2_clicked();

private:
    Ui::Widget *ui;

    FileAccessTask1 fileAccessTask1;
    FileAccessTask2 fileAccessTask2;

    unsigned int currentTextIndex;
};

#endif // WIDGET_H
