#ifndef DATEIZUGRIFFE_AUFG1_H
#define DATEIZUGRIFFE_AUFG1_H

#include <QString>
#include <fstream>

#include "dateien_abstrakte_klasse.h"

using namespace std;

/**
 * @brief The DateizugriffeAufg1 class
 *
 * Format:
 * Zeile 1: BG V11
 * ab Zeile 2: Text (zeilenweise abgelegt)
 *
 */
class FileAccessTask1 : Dateien_abstrakte_klasse
{
public:
    FileAccessTask1();

    void saveFile(QString fileName, QString text);
    bool loadFile(QString fileName, QString &textToOutput);
};

#endif // DATEIZUGRIFFE_AUFG1_H
