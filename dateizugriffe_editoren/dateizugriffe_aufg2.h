#ifndef DATEIZUGRIFFEAUFG2_H
#define DATEIZUGRIFFEAUFG2_H

#include <fstream>
#include <vector>
#include <QString>
#include <QStringList>

#include "dateien_abstrakte_klasse.h"

using namespace std;

/**
 * @brief The DateizugriffeAufg2 class
 *
 * Format:
 * Zeile 1: BG V12
 * Ziele 2: Anzahl Zeilen Text 1
 * ab Zeile 3: Text 1 (zeilenweise abgelegt)
 *
 */
class FileAccessTask2 : Dateien_abstrakte_klasse
{
public:
    FileAccessTask2();

    void appendText(const QString &text);
    bool setText(unsigned int number, const QString &text);
    bool getText(unsigned int number, QString &text);

    void saveFile(QString fileName);
    bool loadFile(QString fileName);
    bool loadFile(QString fileName, QString &textToOutput);

    unsigned int getTextListSize();

private:
    vector<QString> textList;
};

#endif // DATEIZUGRIFFEAUFG2_H
