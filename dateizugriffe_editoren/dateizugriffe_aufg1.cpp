
#include "dateizugriffe_aufg1.h"

FileAccessTask1::FileAccessTask1(){

}

void FileAccessTask1::saveFile(QString fileName, QString text){

    fstream fileHandle;
    string tempString;
    tempString = fileName.toStdString();
    fileHandle.open(tempString.c_str(), ios::out);
    fileHandle << "BG V11" << endl;
    tempString = text.toStdString();
    fileHandle << tempString << endl;
    fileHandle.close();
}

bool FileAccessTask1::loadFile(QString fileName, QString &textToOutput){

    fstream fileHandle;
    string textIn;

    string tempString;
    tempString = fileName.toStdString();
    fileHandle.open(tempString.c_str(), ios::in);
    getline(fileHandle, textIn);
    if(0 == textIn.compare("BG V11")){
        while (!fileHandle.eof()){
            getline(fileHandle, textIn);
            textToOutput.append(textIn.c_str());
        }
        fileHandle.close();
        return true;
    }
    fileHandle.close();
    return false;
}
