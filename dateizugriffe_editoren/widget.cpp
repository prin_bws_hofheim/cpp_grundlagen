#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent) :
    QWidget(parent),
    ui(new Ui::Widget){
    ui->setupUi(this);

    currentTextIndex = 0;
}

Widget::~Widget(){
    delete ui;
}

void Widget::on_btnSave_clicked(){
    QString filename;
    QString text;
    unsigned int size = 0;

    filename = QFileDialog::getSaveFileName(this, "Datei speichern", "~/",tr("Data files (*.dat);;Text files (*.txt);;XML files (*.xml)") );
    text = ui->pteText->toPlainText();

    if(ui->rbtOneEntry->isChecked())
        fileAccessTask1.saveFile(filename, text);
    if(ui->rbtManyEntrys->isChecked()){
        size = fileAccessTask2.getTextListSize();
        if(size <= currentTextIndex){
            fileAccessTask2.appendText(ui->pteText->toPlainText());
        }
        else{
            if(0 == currentTextIndex){
                fileAccessTask2.setText(currentTextIndex, ui->pteText->toPlainText());
            }
        }

        fileAccessTask2.saveFile(filename);
    }
}

void Widget::on_btnLoad_clicked(){
    QString filename;
    QString text;

    filename = QFileDialog::getOpenFileName(this, "Datei speichern", "~/",tr("Data files (*.dat);;Text files (*.txt);;XML files (*.xml)") );

    if(ui->rbtOneEntry->isChecked()){
        fileAccessTask1.loadFile(filename, text);
        ui->pteText->clear();
        ui->pteText->setPlainText(text);
        currentTextIndex = 0;
    }

    if(ui->rbtManyEntrys->isChecked()){
        currentTextIndex = 0;
        fileAccessTask2.loadFile(filename);
        fileAccessTask2.getText(0, text);
        ui->pteText->setPlainText(text);
        ui->btnBack->setEnabled(false);
        ui->btnForward->setEnabled(true);
    }
}

void Widget::on_rbtOneEntry_clicked(){
    ui->btnBack->setEnabled(false);
    ui->btnForward->setEnabled(false);
    currentTextIndex = 0;
}

void Widget::on_rbtManyEntrys_clicked(){

    ui->btnBack->setEnabled(true);
    ui->btnForward->setEnabled(true);
    currentTextIndex = 0;

    if(0 == currentTextIndex){
        ui->btnBack->setEnabled(false);
    }
    if(fileAccessTask2.getTextListSize() < currentTextIndex){
        ui->btnForward->setEnabled(false);
    }
}

void Widget::on_btnBack_clicked(){

    QString text;

    unsigned int size = 0;

    size = fileAccessTask2.getTextListSize();
    if(size <= currentTextIndex){
        fileAccessTask2.appendText(ui->pteText->toPlainText());
    }
    else{
        if(0 == currentTextIndex){
            fileAccessTask2.setText(currentTextIndex, ui->pteText->toPlainText());
        }
    }

    if(0 < currentTextIndex){
        currentTextIndex--;

        fileAccessTask2.getText(currentTextIndex, text);
        ui->pteText->clear();
        ui->pteText->setPlainText(text);
    }
    if(0 < currentTextIndex){
        ui->btnBack->setEnabled(true);
    }
    else{
        ui->btnBack->setEnabled(false);
    }
}

void Widget::on_btnForward_clicked(){

    QString text;

    unsigned int size = 0;

    size = fileAccessTask2.getTextListSize();

    if((size == 0)|| (size <= currentTextIndex)){
        fileAccessTask2.appendText(ui->pteText->toPlainText());
    }
    else{
        fileAccessTask2.setText(currentTextIndex, ui->pteText->toPlainText());
    }

    size = fileAccessTask2.getTextListSize();

    if(fileAccessTask2.getTextListSize() > currentTextIndex){
        currentTextIndex++;

        fileAccessTask2.getText(currentTextIndex, text);
        ui->pteText->clear();
        ui->pteText->setPlainText(text);
        ui->btnBack->setEnabled(true);
    }
}

void Widget::on_pushButton_clicked()
{
    ui->pteText->appendPlainText(QString::number(M_PI));
}

void Widget::on_pushButton_2_clicked()
{
    ui->pteText->appendPlainText(QString::number(M_E));
}
