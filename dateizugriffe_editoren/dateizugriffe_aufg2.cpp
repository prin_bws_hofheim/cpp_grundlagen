#include "dateizugriffe_aufg2.h"

FileAccessTask2::FileAccessTask2(){

}

void FileAccessTask2::appendText(const QString &text){

    textList.push_back(text);
}

bool FileAccessTask2::setText(unsigned int number, const QString &text){
    if(textList.size() >= number){
        textList.at(number) = text;
        return true;
    }
    return false;
}

bool FileAccessTask2::getText(unsigned int number, QString &text){

    if(textList.size() > number){
        text = textList.at(number);
        return true;
    }
    return false;
}

unsigned int FileAccessTask2::getTextListSize(){
    return textList.size();
}

void FileAccessTask2::saveFile(QString fileName){

    ofstream fileHandle;
    string textIn;
    QString tempText;
    QStringList tempList;
    int lines = 0;
    int posEOL = -1;

    if(textList.size() == 0)
        return; // keine Texte

    fileHandle.open(fileName.toStdString(), ios::out);
    fileHandle << "BG V12";

    for(int i = 0; i < textList.size();i++)
    {
        tempText = textList[i];
        posEOL = tempText.lastIndexOf('\n');
        if(posEOL == tempText.length()-1)
            tempText = tempText.left(posEOL);

        tempList = tempText.split('\n');
        fileHandle << endl << tempList.count() << endl;
        fileHandle << tempText.toStdString();
    }

    fileHandle.close();
}

bool FileAccessTask2::loadFile(QString fileName){

    ifstream fileHandle;
    string textIn;
    QString tempText;
    int lines = 0;

    textList.clear();
    fileHandle.open(fileName.toStdString(), ios::in);
    if(!fileHandle.is_open())
        return false;

    getline(fileHandle, textIn);
    if(0 == textIn.compare("BG V12")){
        while (!fileHandle.eof()){
            getline(fileHandle, textIn);
            tempText = textIn.c_str();
            lines = tempText.toInt();
            tempText.clear();
            for(int i = 0; i < lines; i++){
                getline(fileHandle, textIn);
                tempText.append(textIn.c_str());
                if(i < lines)
                    tempText.append('\n');
            }
            if(!tempText.isEmpty())
                textList.push_back(tempText);
            tempText.clear();
        }
        fileHandle.close();
        return true;
    }
    fileHandle.close();
    return false;
}

bool FileAccessTask2::loadFile(QString fileName, QString &textToOutput)
{

}
