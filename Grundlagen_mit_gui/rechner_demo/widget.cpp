/**
 * @file widget.cpp
 *
 * Demo für Funktionen/Methoden
 *
 *
 * @todo Methode für zweite Spinbox schreiben
 */

#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

/**
 * @brief Inhalt aus oberer Spinbox holen
 * @return Inhalt aus Spinbox
 *
 */
int Widget::getZahl1()
{
    int eingabe = 0;

    eingabe = ui->spin_oben->value();
    return eingabe;
}

/**
 * @brief Ergebnis in Spin_oben schreiben
 * @param value Inhalt für spinbox
 */
void Widget::setZahl1(int value)
{
    ui->spin_oben->setValue(value);
}

/**
 * @brief Zahl nach Wunsch (oben, unten , links, rechts) auslesen undd zurück geben
 * @param nr was ist gewünscht (1 = oben links, 2 = unten links, 3 = oben rechts, 4 = unten rechts)
 * @return Ergebnis
 */
int Widget::getZahlAuswahl(int nr)
{

}

/**
 * @brief Zahl, die im oberen lineEdit steht auslesen, konvertieren und zurück geben
 * @return Ergenis
 */
int Widget::getZahlAusString_oben()
{
    QString eingabeText;//braucht kein init, macht der automatisch
    int eingabeNr = 0;

    eingabeText = ui->edt_oben->text();
    eingabeNr = eingabeText.toInt();
    return eingabeNr;
}


/**
 * @brief "der Taschenrechner"
 */
void Widget::on_btnRechneSpinbox_clicked()
{
    int zahl1 = 0;
    int zahl2 = 0;
    int summe = 0;

    zahl1 = getZahl1();

    summe = zahl1 + zahl2;
    setZahl1(summe);
}

