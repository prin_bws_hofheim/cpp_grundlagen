var indexSectionsWithContent =
{
  0: "agmosuw~",
  1: "w",
  2: "u",
  3: "mw",
  4: "gmosw~",
  5: "u",
  6: "a"
};

var indexSectionNames =
{
  0: "all",
  1: "classes",
  2: "namespaces",
  3: "files",
  4: "functions",
  5: "variables",
  6: "pages"
};

var indexSectionLabels =
{
  0: "Alle",
  1: "Klassen",
  2: "Namensbereiche",
  3: "Dateien",
  4: "Funktionen",
  5: "Variablen",
  6: "Seiten"
};

