#include "widget.h"
#include "ui_widget.h"

Widget::Widget(QWidget *parent)
    : QWidget(parent)
    , ui(new Ui::Widget)
{
    ui->setupUi(this);
}

Widget::~Widget()
{
    delete ui;
}

int Widget::getZahl1()
{
    int eingabe = 0;

    eingabe = ui->spin_oben->value();
    return eingabe;
}

int Widget::getZahl2()
{
    int eingabe = 0;

    eingabe = ui->spin_unten->value();
    return eingabe;
}

void Widget::setZahl1(int value)
{
    ui->spin_oben->setValue(value);
}

int Widget::getZahlAuswahl(int nr)
{
    int eingabeNr = 0;

    switch(nr)
    {
    case 1:
        eingabeNr = ui->spin_oben->value();
        break;
    case 2:
        eingabeNr = ui->spin_unten->value();
        break;
    default:
        eingabeNr = 0;
    }
    return eingabeNr;
}

int Widget::getZahlAusString_oben(bool &ok)
{
    QString eingabeText;//braucht kein init, macht der automatisch
    int eingabeNr = 0;
    bool *ok2 = new bool;
    ok = false;

    eingabeText = ui->edt_oben->text();
    eingabeNr = eingabeText.toInt(ok2);
    ok = *ok2;//StringToInt braucht das
    delete ok2; //gegen Speicherlecks

    return eingabeNr;
}



void Widget::on_btnRechneSpinbox_clicked()
{
    int zahl1 = 0;
    int zahl2 = 0;
    int summe = 0;

    zahl1 = getZahl1();
    zahl2 = getZahl2();

    summe = zahl1 + zahl2;
    setZahl1(summe);
}


void Widget::on_btnRechneLineEdit_clicked()
{
    int zahl1 = 0;
    int zahl2 = 0;
    int summe = 0;
    bool ok;

    zahl1 = getZahlAusString_oben(ok);
    qDebug() << "Zahl1: " << zahl1;

    summe = zahl1 + zahl2;
    if(ok)
        setZahl1(summe);
}

