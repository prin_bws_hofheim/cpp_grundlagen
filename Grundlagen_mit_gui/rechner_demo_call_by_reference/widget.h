#ifndef WIDGET_H
#define WIDGET_H

#include <QWidget>
#include <QDebug>

QT_BEGIN_NAMESPACE
namespace Ui { class Widget; }
QT_END_NAMESPACE

class Widget : public QWidget
{
    Q_OBJECT

public:
    Widget(QWidget *parent = nullptr);
    ~Widget();

private slots:
    void on_btnRechneSpinbox_clicked();
    int getZahlAusString_oben(bool &ok);
    int getZahl1();
    void setZahl1(int value);
    int getZahlAuswahl(int nr);
    void on_btnRechneLineEdit_clicked();

    int getZahl2();
private:
    Ui::Widget *ui;
};
#endif // WIDGET_H
